'''
The Letter Frequency Analysis Attack is used in the following python program.

It will first print the stastical frequency of every letter.

I will then point out the frequency each letter matches on Table 1.1.
'''

ciphertext_first = """
lrvmnir bpr sumvbwvr jx bpr lmiwv yjeryrkbi jx qmbm wi
bpr xjvni mkd ymibrut jx irhx wi bpr riirkvr jx
ymbinlmtmipw utn qmumbr dj w ipmhh but bj rhnvwdmbr bpr
yjeryrkbi jx bpr qmbm mvvjudwko bj yt wkbrusurbmbwjk
lmird jk xjubt trmui jx ibndt\n
"""

ciphertext_two = """
wb wi kjb mk rmit bmiq bj rashmwk rmvp yjeryrkb mkd wbi
iwokwxwvmkvr mkd ijyr ynib urymwk nkrashmwkrd bj ower m
vjyshrbr rashmkmbwjk jkr cjnhd pmer bj lr fnmhwxwrd mkd
wkiswurd bj invp mk rabrkb bpmb pr vjnhd urmvp bpr ibmbr
jx rkhwopbrkrd ywkd vmsmlhr jx urvjokwgwko ijnkdhrii
ijnkd mkd ipmsrhrii ipmsr w dj kjb drry ytirhx bpr xwkmh
mnbpjuwbt lnb yt rasruwrkvr cwbp qmbm pmi hrxb kj djnlb
bpmb bpr xjhhjcwko wi bpr sujsru msshwvmbwjk mkd
wkbrusurbmbwjk w jxxru yt bprjuwri wk bpr pjsr bpmb bpr
riirkvr jx jqwkmcmk qmumbr cwhh urymwk wkbmvb
"""

ciphertext = ciphertext_first + ciphertext_two

ciphertext_len = len(ciphertext)

print("ciphertext_len: " + str(ciphertext_len))

'''
The following while loop calculates the raw frequency of each character

in the ciphertext.
'''

i = 0

freq_table = [0] * 26

while i < ciphertext_len:

    if ciphertext[i].isspace() == True:
        
        i = i + 1

        continue
    
    freq_table[ord(ciphertext[i]) - ord('a')] += 1

    i = i + 1

print("freq_table:\n" + str(freq_table))

'''
The following while loop calculates the number of non whitespace characters

in the ciphertext.
'''

z = 0

non_whitespace_sum = 0

while z < len(freq_table):

    non_whitespace_sum += freq_table[z]

    z += 1

print("character sum: " + str(non_whitespace_sum))

'''
The following while loop calculates the number of whitespace characters

in the ciphertext.
'''

z = 0

whitespace_sum = 0

while z < len(ciphertext):

    if ciphertext[z].isspace() == True:

        whitespace_sum += 1

    z += 1

print("whitespace sum: " + str(whitespace_sum))

'''
The following while loop calculates the ratio of the frequency of each character

relative to the length of the ciphertext.
'''

i = 0

while i < 26:

    freq_table[i] /= non_whitespace_sum

    i = i + 1

'''
The following while loop prints the ratio of the frequency of each character

relative to the length of the ciphertext
'''

i = 0

char = 'a'

while i < 26:

    print("frequency " + "of " + char + ": " + str(freq_table[i]))

    i += 1

    char = chr(ord(char) + 1)

'''
The following while loop adds the sum of the ratio of the frequences in the

freq_table.
'''

i = 0

sum = 0

while i < 26:

    sum += freq_table[i]

    i += 1

print("ratio of frequency sum: " + str(sum))

char = 'a'

while i < 26:

    print(char + " = " + str(freq_table[i]) + "\n")

    char = chr(ord(char) + 1)

    i += 1

'''
The following while loop deciphers the ciphertext into its plaintext form using the

substitution_table below.
'''
print("\nPlaintext:\n")

substitution_table = [

        'x',

        't',

        'w',

        'd',

        'v',

        'q',

        'z',

        'l',

        's',

        'o',

        'n',

        'b',

        'a',

        'u',

        'g',

        'h',

        'k',

        'e',

        'p',

        'y',

        'r',

        'c',

        'i',

        'f',

        'm',

        'z'

]

i = 0

while i < len(ciphertext_first):

    if ciphertext_first[i].isspace() == True:
    
        print(ciphertext_first[i],end='')

        i += 1

        continue


    print(substitution_table[ord(ciphertext_first[i]) - ord('a')],end='')

    i += 1

i = 0

while i < len(ciphertext_two):
    
    if ciphertext_two[i].isspace() == True:

        print(ciphertext_two[i],end='')

        i += 1
        
        continue

    print(substitution_table[ord(ciphertext_two[i]) - ord('a')],end='')
    
    i += 1


