'''
The Letter Frequency Analysis Attack is used in the following python program.

It will first print the stastical frequency of every letter.

I will then point out the frequency each letter matches on Table 1.1.
'''

ciphertext = """
xultpaajcxitltlxaarpjhtiwtgxktghidhipxciwtvgtpilpit
ghlxiwiwtxgqadds
"""

ciphertext_len = len(ciphertext)

print("ciphertext_len: " + str(ciphertext_len))

'''
The following while loop calculates the raw frequency of each character

in the ciphertext.
'''

i = 0

freq_table = [0] * 26

while i < ciphertext_len:

    if ciphertext[i].isspace() == True:
        
        i = i + 1

        continue
    
    freq_table[ord(ciphertext[i]) - ord('a')] += 1

    i = i + 1

print("freq_table:\n" + str(freq_table))

'''
The following while loop calculates the number of non whitespace characters

in the ciphertext.
'''

z = 0

non_whitespace_sum = 0

while z < len(freq_table):

    non_whitespace_sum += freq_table[z]

    z += 1

print("character sum: " + str(non_whitespace_sum))

'''
The following while loop calculates the number of whitespace characters

in the ciphertext.
'''

z = 0

whitespace_sum = 0

while z < len(ciphertext):

    if ciphertext[z].isspace() == True:

        whitespace_sum += 1

    z += 1

print("whitespace sum: " + str(whitespace_sum))

'''
The following while loop calculates the ratio of the frequency of each character

relative to the length of the ciphertext.
'''

i = 0

while i < 26:

    freq_table[i] /= non_whitespace_sum

    i = i + 1

'''
The following while loop prints the ratio of the frequency of each character

relative to the length of the ciphertext
'''

i = 0

char = 'a'

while i < 26:

    print(char + " = " + str(freq_table[i]))

    i += 1

    char = chr(ord(char) + 1)

'''
The following while loop adds the sum of the ratio of the frequences in the

freq_table.
'''

i = 0

sum = 0

while i < 26:

    sum += freq_table[i]

    i += 1

print("ratio of frequency sum: " + str(sum))

char = 'a'

while i < 26:

    print(char + " = " + str(freq_table[i]) + "\n")

    char = chr(ord(char) + 1)

    i += 1

'''
The following while loop counts the number of unique letters in the cihpertext. 

The number of unique letters that must be identified through a frequency count minus 1 is the number

of letters that must be deduced through Letter Frequency Analysis to verify our guess of the key is correct.

Based on the number of unique letters in the ciphertext, 18, we need to identify 17 unique letters through

Letter Frequency Analysis since discovering 17 will allow us to know the 18th remaining letter without having

to perform Letter Frequency Analysis on the remaining 18th letter.
'''

i = 0

sum = 0

while i < 26:

    if freq_table[i] != 0:

        sum += 1

    i += 1

print("Number of unique letters in frequency table: " + str(sum))

'''
The following shifts all letters by 15, which is the key for this problem.
'''

s = 15

i = 0

dec = ""

while i < ciphertext_len:

    if ciphertext[i].isspace() == True:

        i += 1

        continue
    
    mod_operand = (ord(ciphertext[i])  - ord('a') - s ) % 26 

    dec_letter = chr( mod_operand + ord('a'))

    dec += dec_letter

    i += 1

print("\nThe following is the plaintext when decrypted with key k = 15\n")

print("plaintext: " + dec + " ; k = " + str(s) + "\n")


'''
The following shifts all letters in the ciphertext by all 25 possible shift numbers

that change the output of the text.
'''

s = 1

i = 0

dec = ""

while s <= 25:

    i = 0

    dec = ""

    while i < ciphertext_len:

        if ciphertext[i].isspace() == True:

            i += 1

            continue
        
        mod_operand = (ord(ciphertext[i])  - ord('a') - s ) % 26 

        dec_letter = chr( mod_operand + ord('a'))

        dec += dec_letter

        i += 1

    print("plaintext: " + dec + " ; k = " + str(s))

    s += 1
