'''
Decryption cipher:

d_k(y) = x = a^(-1) * (y - b) mod 26

The multiplicative inverse a^(-1) is 15 since

7 * 15 mod 26 is the same as 1 mod 26

Now that the multiplicative inverse has been found

each letter must be decrypted according to the function

d_k(y) above.
'''

ciphertext = "falszztysyjzyjkywjrztyjztyynaryjkyswarztyegyyj"

ciphertext_len = len(ciphertext)

i = 0

dec = ""

while i  < ciphertext_len:

    if ciphertext[i].isspace():

        i += 1

        continue
   

    shift = (ord(ciphertext[i]) - ord('a')) # the following variable shift == (y-b) in the Affine Cipher formula

    dec += chr( ( 15 * (shift - 22)  % 26 ) + ord('a') )

    i += 1

print(dec)

